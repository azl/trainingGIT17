# Daftar Perintah Git #

1. Membuat repository baru

git init nama-folder

2. Melihat status working folder

git status

3. Menambahkan perubahan ke staging area

git add .

4. Menyimpan perubahan ke repository

git commit -m "pesan / keterangan commit"

![Gambar konsep staging area](img/konsep-staging-area.jpg)

5. Melihat history perubahan

git log
git log --oneline

6. Melihat perbedaan (difference) antar versi

git diff commitId1..commitId2

7. Menghapus perubahan di staging

git reset

8. Menghapus perubahan di working dan staging (!!bahaya!!)

git reset --hard

9. Mendaftarkan remote repository

git remote add namaremote urlremote

git remote add gitlab git@gitlab.com:endymuhardin/training-git-2017-01.git

10. Mengambil repo dari remote

git clone git@gitlab.com:endymuhardin/training-git-2017-01.git